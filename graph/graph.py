import os, sys

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

class ColorButton (QToolButton) :
    def __init__ (self, page, name, color) :
        super().__init__(page)
        self.name = name
        self.color = color

        self.setToolTip (name)
        page.addWidget (self)

        pixmap = QPixmap (12, 12)
        pixmap.fill (Qt.transparent)

        painter = QPainter (pixmap)
        painter.setPen (Qt.NoPen)
        painter.setBrush (color)
        painter.drawEllipse (0, 0, 12, 12)
        painter.end ()

        icon = QIcon (pixmap)
        self.setIcon (icon)

    def mousePressEvent (self, event) :

        drag = QDrag (self)

        mimeData = QMimeData ()
        mimeData.setText (self.name)
        mimeData.setColorData (self.color)

        drag.setMimeData (mimeData)

        drop = drag.exec_ ()

# --------------------------------------------------------------------------

class ToolButton (QToolButton) :

    def __init__ (self, page, name) :
        super().__init__(page)
        self.name = name

        self.setText (name)
        self.setToolTip (name)
        page.addWidget (self)

    def mousePressEvent (self, event) :

        drag = QDrag (self)

        mimeData = QMimeData ()
        mimeData.setData ("application/x-tool", bytearray (self.name, "ascii"))

        drag.setMimeData (mimeData)
        # drag.setPixmap (self.getPixmap ())
        drop = drag.exec_ (Qt.MoveAction | Qt.CopyAction | Qt.LinkAction)

# --------------------------------------------------------------------------


class Block (QGraphicsRectItem) :

    def __init__ (self) :
        super().__init__()
        self.setAcceptDrops (True)

    def dragEnterEvent (self, event) :
        mime = event.mimeData ()
        if mime.hasColor () or mime.hasFormat ("application/x-tool") :
            event.acceptProposedAction ()
        else :
           event.setAccepted (False)

    def dropEvent (self, event) :
        mime = event.mimeData ()
        if mime.hasColor () :
           color = mime.colorData ()
           self.setBrush (color)
        elif mime.hasFormat ("application/x-tool") :
            name = str (mime.data ("application/x-tool"), "ascii")
            self.setBrush (QColor ("lime"))
            self.setToolTip (name)
            if name == "rectangle" :
               block = Block ()
               block.setRect (0, 0, 100, 60)
               block.setPos (0, 0)
               block.setPen (QColor ("lime"))
               block.setBrush (QColor ("orange"))
               block.setFlags (QGraphicsItem.ItemIsMovable | QGraphicsItem.ItemIsSelectable)

               block.setParentItem (self)
               block.setPos (event.scenePos() - self.scenePos())

# --------------------------------------------------------------------------

class Window (QMainWindow) :

    def __init__ (self) :
        super().__init__ ()

        self.widget = QWidget ()
        self.setCentralWidget (self.widget)

        self.layout = QVBoxLayout (self.widget)

        # palette

        self.palette = QTabWidget (self)
        self.layout.addWidget (self.palette)

        "color buttons"
        self.colorPage = QToolBar (self)
        self.palette.addTab (self.colorPage, "Colors")


        for name in ["red", "green", "blue", "yellow", "orange"] :
            ColorButton (self.colorPage, name, QColor (name))

        "tool buttons"
        self.toolPage = QToolBar ()
        self.palette.addTab (self.toolPage, "Tools")

        names = [ "rectangle", "ellipse", "line"]
        for name in names :
            ToolButton (self.toolPage, name)


        # graphics view

        self.view = QGraphicsView (self)
        self.layout.addWidget (self.view)

        self.scene = QGraphicsScene (self)
        self.view.setScene (self.scene)
        self.scene.setSceneRect (0, 0, 800, 600)


        self.scene.addLine (100, 100, 200, 200, QPen (QColor ("red"), 3))

        block = Block ()
        block.setPen ( QPen (QColor ("blue"), 1) )
        block.setBrush ( QBrush (QColor ("yellow") ) )
        block.setPos (100, 100)
        block.setRect (0, 0, 240, 120)
        block.setToolTip ("block")
        block.setFlags (QGraphicsItem.ItemIsMovable | QGraphicsItem.ItemIsSelectable)
        self.scene.addItem (block)


        for i in range (2) :
            item = QGraphicsEllipseItem ()
            item.setPen ( QColor ("blue") )
            item.setBrush ( QColor ("cornflowerblue") )
            item.setPos (40 + 120*i, 40)
            item.setRect (0, 0, 40, 40)
            item.setToolTip ("item " + str (i+1))
            item.setFlags (QGraphicsItem.ItemIsMovable | QGraphicsItem.ItemIsSelectable)
            item.setParentItem (block)


app = QApplication (sys.argv)
win = Window ()
win.show ()
sys.exit (app.exec_ ())












