import sys

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtSql import *

class DbView (QMainWindow):

   def __init__ (self, db, parent = None) :
       super (DbView, self).__init__ (parent)
       self.db = db

       self.tree = QTreeWidget (self)
       self.table = QTableView (self)

       splitter = QSplitter (self)
       splitter.setOrientation (Qt.Horizontal)
       splitter.addWidget (self.tree)
       splitter.addWidget (self.table)

       self.setCentralWidget (splitter)

       model = QSqlTableModel (self, self.db)
       model.setTable ("colors")
       model.select ()

       self.showTables ()
       self.table.setModel (model)


   def showTables (self) :
       for name in db.tables () :
           table_node = QTreeWidgetItem (self.tree)
           table_node.setText (0, name)

           query = db.exec_ ("SELECT * FROM " + name)
           rec = query.record ()
           for k in range (rec.count ()) :
               txt = rec.fieldName (k)

               field = rec.field (k)
               field_type = field.type ()
               type_name = "?"
               if field_type == QVariant.String :
                  type_name = "string"
               elif field_type == QVariant.Int :
                  type_name = "int"
               txt = txt + " : " + type_name

               # txt = txt + " : " + str (field.precision ())
               # txt = txt + " : " + str (field.defaultValue ())

               column_node = QTreeWidgetItem (table_node)
               column_node.setText (0, txt)


db = QSqlDatabase.addDatabase ("QSQLITE")
# db.setDatabaseName (":memory:")
db.setDatabaseName ("test.sqlite")
db.open ()

# cmd = "DROP TABLE colors"
# db.exec_ (cmd)

"create table"

cmd = "CREATE TABLE IF NOT EXISTS colors"
cmd = cmd + " (name TEXT PRIMARY KEY, red INTEGER, green INTEGER, blue INTEGER)"
db.exec_ (cmd)

"insert"

cmd = "INSERT INTO colors (name, red, green, blue) VALUES (\"blue\", 0, 0, 255)"
db.exec_ (cmd)

"insert 2"

cmd = "INSERT INTO colors (name, red, green, blue) VALUES (\"red\", 255, 0, 0)"
db.exec_ (cmd)

"insert 3"

insert = QSqlQuery (db)
insert.prepare ("INSERT INTO colors (name, red, green, blue) VALUES (:name, :red, :green, :blue)")

insert.bindValue (":name",  "green")
insert.bindValue (":red",   QVariant (0))
insert.bindValue (":green", QVariant (255))
insert.bindValue (":blue",  QVariant (0))

insert.exec_ ()

"select"

query = db.exec_ ("SELECT * FROM colors ORDER BY name")
while query.next () :
    print (query.value ("name"), query.value ("red"), query.value (2), query.value (3))

"Window"

app = QApplication (sys.argv)
win = DbView (db)
win.show ()
app.exec_ ()

