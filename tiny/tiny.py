import os, sys, subprocess

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

try:
   from clang.cindex import *
   # Fedora : dnf install python3-clang
   use_cindex = True
except :
   use_cindex = False
   print ("Missing clang.cindex")

# --------------------------------------------------------------------------

def bytearray_to_str (b) :
    return str (b, "ascii", errors="ignore")

# --------------------------------------------------------------------------

class Window (QMainWindow) :

   def __init__ (self, parent = None) :
       super (Window, self).__init__ (parent)

       # variables

       self.editors = { }

       self.file_names = { } # cislo souboru -> jmeno souboru (i s adresarem)

       # user interface

       self.tree = QTreeWidget (self)

       self.tabs = QTabWidget (self)

       # self.edit = QTextEdit (self)
       # self.edit.setLineWrapMode (QTextEdit.NoWrap)

       # self.tabs.addTab (self.edit, "edit")

       self.output = QTextEdit (self)
       self.output.setLineWrapMode (QTextEdit.NoWrap)

       hsplitter = QSplitter (self)
       hsplitter.setOrientation (Qt.Horizontal)
       hsplitter.addWidget (self.tree)
       hsplitter.addWidget (self.tabs)

       vsplitter = QSplitter (self)
       vsplitter.addWidget (hsplitter)
       vsplitter.addWidget (self.output)
       vsplitter.setOrientation (Qt.Vertical)

       hsplitter.setStretchFactor (0, 1)
       hsplitter.setStretchFactor (1, 3)

       vsplitter.setStretchFactor (0, 3)
       vsplitter.setStretchFactor (1, 1)

       self.setCentralWidget (vsplitter)

       # menu

       fileMenu = self.menuBar().addMenu ("&File")

       act = QAction ("&Open...", self)
       act.setShortcut ("Ctrl+O")
       act.triggered.connect (self.openFile)
       fileMenu.addAction (act)

       act = QAction ("&Save", self)
       act.setShortcut ("Ctrl+S")
       act.triggered.connect (self.saveFile)
       fileMenu.addAction (act)

       act = QAction ("Save &As...", self)
       act.setShortcut ("Ctrl+Shift+S")
       act.triggered.connect (self.saveFileAs)
       fileMenu.addAction (act)

       fileMenu.addSeparator ()

       act = QAction ("&Quit", self)
       act.setShortcut ("Ctrl+Q")
       act.triggered.connect (self.close)
       fileMenu.addAction (act)

       editMenu = self.menuBar().addMenu ("&Edit")

       act = QAction ("set &Font", self)
       act.triggered.connect (self.selectFont)
       editMenu.addAction (act)

       runMenu = self.menuBar().addMenu ("&Run")

       act = QAction ("&Compile and Run", self)
       act.setShortcut ("F5")
       act.triggered.connect (self.runFile)
       runMenu.addAction (act)

       act = QAction ("Clang &Declarations", self)
       act.setShortcut ("F6")
       act.triggered.connect (self.declarations)
       runMenu.addAction (act)

   def openEditor (self, file_name) :
       file_name = os.path.abspath (file_name)
       if file_name in self.editors :
          edit = self.editors [file_name]
       else:
          dir_name, short_name = os.path.split (file_name)

          edit = QTextEdit (self)
          edit.setLineWrapMode (QTextEdit.NoWrap)
          self.tabs.addTab (edit, short_name)

          edit.fileName = file_name
          self.editors [file_name] = edit

          with open (file_name) as f :
              text = f.read ()
          edit.setPlainText (text)
       self.tabs.setCurrentWidget (edit)

   def openFile (self) :
       file_name, ok = QFileDialog.getOpenFileName (self, "Open File")
       if file_name != "" :
          self.openEditor (file_name)

   def writeEditor (self, edit, file_name) :
       edit.fileName = file_name
       text = edit.toPlainText ()
       with open (file_name, "w") as f :
          f.write (text)

   def getEditor (self) :
       return self.tabs.currentWidget ()

   def saveFile (self) :
       edit = self.getEditor ()
       if edit != None :
          self.writeEditor (edit, edit.fileName)

   def saveFileAs (self) :
       edit = self.getEditor ()
       if edit != None :
          file_name, ok = QFileDialog.getSaveFileName (self, "Save File As", self.fileName)
          if file_name != "" :
             self.writeFile (edit, file_name)

   def selectFont (self) :
       font, ok = QFontDialog.getFont (self.tree.font (), self)
       if ok :
          self.tree.setFont (font)
          self.tabs.setFont (font)
          self.output.setFont (font)

   def runFile (self) :
       edit = self.getEditor ()
       if edit != None :
          file_name = edit.fileName
          self.output.clear ()
          cmd = "gcc " + file_name + " -lstdc++ -o run.bin && ./run.bin ; rm ./run.bin"
          self.output.append (cmd)
          proc = subprocess.Popen (cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
          for line in proc.stderr :
              self.output.append (bytearray_to_str (line))
          for line in proc.stdout :
              self.output.append (bytearray_to_str (line))

   "CLANG"

   def get_file_number (self, location) :
       # print ("LOCATION", location)
       cxfile = location.file
       if cxfile == None :
          result = 0
       elif cxfile in self.cx_files :
          result = self.cx_files [cxfile] # vyzvednu cislo souboru
       else :
          file_name = cxfile
          inx = len (self.file_names) + 1 # od 1
          self.file_names [inx] = file_name # tabulka cislo souboru -> jmeno souboru
          result = inx
       return result

   def get_file_name (self, file_num) :
       result = ""
       if file_num in self.file_names :
          result = self.file_names [file_num]

   def print_branch (self, target, cursor, level = 0) :
       txt = cursor.kind.name + ": " + cursor.spelling
       node = QTreeWidgetItem (target)
       node.setText (0, txt)

       location = cursor.location
       if location :
          node.file_num = self.get_file_number (location) # location -> cislo souboru
          node.line     = location.line
          node.col      = location.column

       if level < 10 :
           for item in cursor.get_children () :
              # if item.kind != CursorKind.COMPOUND_STMT :
              self.print_branch (node, item, level + 1)

   def declarations (self) :
       edit = self.getEditor ()
       if edit != None and use_cindex :
          file_name = edit.fileName

          index = Index.create ()
          tu = index.parse (file_name) # Translation Unit

          for item in tu.diagnostics :
              print ("MESSAGE:", str (item))

          self.tree.clear ()
          cursor = tu.cursor
          self.print_branch (self.tree, cursor)

# --------------------------------------------------------------------------

if __name__ == "__main__" :
   app = QApplication (sys.argv)
   win = Window ()
   win.show ()
   win.openEditor ("example.cc")
   app.exec_ ()
