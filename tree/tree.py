import os, sys, time, hashlib
from stat import *

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

class Window (QMainWindow) :

    def __init__ (self) :
        super().__init__ ()

        self.setFont (QFont ("", 20))

        self.tree = QTreeWidget (self)
        self.tree.setHeaderLabels (["name", "size", "time", "crc"])

        self.tabs = QTabWidget (self)
        self.tabs.addTab (QTextEdit (), "one")
        self.tabs.addTab (QTextEdit (), "two")

        self.info = QTextEdit ()

        self.vsplitter = QSplitter (self)
        self.vsplitter.setOrientation (Qt.Vertical)

        self.splitter = QSplitter (self)
        self.splitter.addWidget (self.tree)
        self.splitter.addWidget (self.tabs)

        self.vsplitter.addWidget (self.splitter)
        self.vsplitter.addWidget (self.info)

        self.vsplitter.setStretchFactor (0, 4) # element 0 ... splitter ... 4
        self.vsplitter.setStretchFactor (1, 1) # element 1 ... info ... 1

        self.setCentralWidget (self.vsplitter)

        self.tree.itemDoubleClicked.connect (self.on_tree_doubleclick)

    def on_tree_doubleclick (self, item : QTreeWidgetItem, column : int) :
        # item.setForeground (0, QColor ("orange"))
        # path = item.path
        path = getattr (item, "path", "")
        if path != "" :
           # v = os.path.split (path)
           # dir_name = v[0]
           # short_name = v[1]
           # (dir_name, short_name) = os.path.split (path)
           dir_name, short_name = os.path.split (path)
           edit = QTextEdit (self)
           inx = self.tabs.addTab (edit, short_name)
           self.tabs.setTabToolTip (inx, path)

           s = ""
           f = open (path, "rb")
           data = f.read (32*1024)
           for c in data :
             s += chr (c)
           f.close ()
           edit.setPlainText (s)

    def simple_scan (self, target, path) :
        names = os.listdir (path)
        names.sort ()

        # for inx in range (len (names)) :
        #    name = names [inx]

        for name in names :
            full_name = os.path.join (path, name)

            node = QTreeWidgetItem (target)
            node.setText (0, name)
            node.setToolTip (0, full_name)
            node.path = full_name

            if os.path.isdir (full_name) :
                node.setForeground (0, QColor ("red"))
                self.scan (node, full_name)
            else :
                node.setForeground (0, QColor ("blue"))

                info = os.stat (full_name)

                size = info [ST_SIZE]
                s = str (size)
                while (len (s)) < 9 :
                    s = " " + s
                if len (s) == 9 :
                    s = s[0:3] + " " + s[3:6] + " " + s[6:9]
                node.setText (1, s)

                t1 = info [ST_MTIME]
                # t2 = time.gmtime (t1)
                t2 = time.localtime (t1)
                t3 = time.strftime ("%Y-%m-%d:%H:%M:%S", t2)
                node.setText (2, t3)

                m = hashlib.md5 ()
                # m = hashlib.sha512 ()
                f = open (full_name, "rb")
                while True :
                    data = f.read (32*1024)
                    if not data :
                        break
                    m.update (data)
                crc = m.hexdigest ()
                node.setText (3, crc)

                item = [full_name, size, t3, crc]
                self.output.append (item)

    def scan (self, target, path) :
        try :
           self.simple_scan (target, path)
        except PermissionError as e :
           print ("Not accessible", path, "(", e, ")")
        except Exception as e :
           print ("Another Error in", path, "(", type (e), str (e), ")")

    def scan_dir (self, path) :
        path = os.path.abspath (path)
        self.output = [ ]
        self.scan (self.tree, path)

        if 0 :
           f = open ("output.txt", "w")
           for item in self.output :
              print (item, file = f)
           f.close ()

# path = "."
path = "../../temp/easyui-tree"

"""
a = [1,2,3,4,5,6,7]
p = a
p[0] = 100
print ("acko", "delka", len(a), "data", a)
c = []
print ("cecko", "delka", len(c), "data", c)
e = None
# print ("ecko", "delka", len(e), "data", e)
"""

app = QApplication (sys.argv)
win = Window ()
win.show ()
win.scan_dir (path)
code = app.exec_ ()
sys.exit (code)












